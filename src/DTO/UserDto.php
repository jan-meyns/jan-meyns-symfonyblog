<?php
declare(strict_types=1);

namespace App\DTO;

class UserDto
{
    /** @var int */
    private $id;

    /** @var string */
    private $email;

    /** @var string[] */
    private $roles = ['ROLE_USER'];

    /** @var string */
    private $password;

    /** @var string */
    private $name;

    /** @var string */
    private $avatarUrl;

    /**
    /* @param string[] $roles
     */
    public function __construct(
        ?int $id = null,
        ?string $email = null,
        ?array $roles = null,
        ?string $password = null,
        ?string $name = null,
        ?string $avatarUrl = null
    )
    {
        $this->id = $id;
        $this->email = $email;
        $this->roles = $roles;
        $this->password = $password;
        $this->name = $name;
        $this->avatarUrl = $avatarUrl;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
    /* @return string[]
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
    /* @param string[] $roles
     */
    public function setRoles(?array $roles): void
    {
        $this->roles = $roles;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->plainPassword = $password;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getAvatarUrl(): ?string
    {
        return $this->avatarUrl;
    }

    public function setAvatarUrl(?string $avatarUrl): void
    {
        $this->avatarUrl = $avatarUrl;
    }
}