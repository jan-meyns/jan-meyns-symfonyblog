<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
    /* @param string[] $roles
     */
    public function createUser(
        ?string $email,
        ?array $roles,
        ?string $password,
        ?string $name,
        ?string $avatarUrl
    ): User {
        if (empty($roles)) {
            $roles = [];
        }

        $user = new User();
        $user
            ->setEmail($email)
            ->setRoles($roles)
            ->setName($name)
            ->setAvatarUrl($avatarUrl);

        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $password
            )
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}