<?php
declare(strict_types=1);

namespace App\Controller;

use App\DTO\UserDto;
use App\Form\RegistrationFormType;
use App\Security\LoginFormAuthenticator;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /** @var string */
    private const FIREWALL_NAME = 'main';

    /** @var GuardAuthenticatorHandler $guardHandler */
    private $guardHandler;

    /** @var LoginFormAuthenticator $authenticator */
    private $authenticator;

    /** @var UserService $userService */
    private $userService;

    public function __construct(
        GuardAuthenticatorHandler $guardHandler,
        LoginFormAuthenticator $authenticator,
        UserService $userService
    )
    {
        $this->guardHandler = $guardHandler;
        $this->authenticator = $authenticator;
        $this->userService = $userService;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request): Response
    {
        $userDto = new UserDto();
        $form = $this->createForm(RegistrationFormType::class, $userDto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->userService->createUser(
                $userDto->getEmail(),
                $userDto->getRoles(),
                $form->get('plainPassword')->getData(),
                $userDto->getName(),
                $userDto->getAvatarUrl()
            );

            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->authenticator,
                self::FIREWALL_NAME
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
